from grass_session import Session
import grass.script as gscript
from grass.pygrass.modules.shortcuts import general as g
import json
import pyproj
import geojson
import functools
import os
import uuid

p3826 = pyproj.Proj(init='epsg:3826')


class Service:
    def __init__(self, gisdb):
        self.grass_session = Session(
            gisdb=gisdb, location="gis-service", create_opts="")
        self.grass_session.open(
            gisdb=gisdb, location="gis-service", create_opts="")
        self.default_layer = []

    def __del__(self):
        self.grass_session.close()

    def load(self, path, name):
        gscript.run_command('r.in.gdal',
                            input=path,
                            output=name,
                            flags='o',
                            overwrite='True')
        self.default_layer.append(name)

    def profile(self, json, resolution=20):
        output_file = 'h2profile-' + str(uuid.uuid4())
        input_coordinates = list(geojson.utils.coords(geojson.loads(json)))
        wgs84 = all((p[0] <= 180 and p[0] >= -180 and
                    p[1] <= 90 and p[1] >= -90) for p in input_coordinates)
        if (wgs84):
            coordinates = [dict(x=t[0], y=t[1]) for t in (
                map(lambda p: p3826(p[0], p[1]), input_coordinates))]
            flaten_coordinates = [str(t[0]) + ',' + str(t[1])
                                  for t in (map(lambda p: p3826(p[0], p[1]), input_coordinates))]
        else:
            coordinates = list(
                map(lambda p: dict(x=p[0], y=p[1]), input_coordinates))
            flaten_coordinates = [str(val)
                                  for p in input_coordinates for val in p]

            g.region(raster='h2_dem')

            gscript.run_command('r.profile',
                                input=','.join(self.default_layer),
                                output=output_file,
                                coordinates=','.join(flaten_coordinates),
                                overwrite='True',
                                resolution=resolution,
                                flags='g',
                                verbose='True')
            profile = []
            with open(output_file) as f:
                for line in f:
                    p = line.split()
                    if p[3] == '*':
                        continue
                    profile.append(dict(x=float(p[0]), y=float(
                        p[1]), dy=float(p[2]), z=float(p[3])))
            os.remove(output_file)
        return dict(coordinates=coordinates, profile=profile)
