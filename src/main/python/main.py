import os, sys, glob, pathlib, configparser
from bottle import route, run, request, response, error
from dem import grass

# It's the workaround for https://github.com/zarch/grass-session/issues/4
# to patch and verify GRASS path (like bin, lib) is set correctly
if sys.platform == "win32":
    print('LD_LIBRARY_PATH:', os.environ['LD_LIBRARY_PATH'])
    os.environ['PATH'] += os.pathsep + os.environ['LD_LIBRARY_PATH']
    print('PATH patched:', os.environ['PATH'])

settings = configparser.ConfigParser(os.environ)
settings.read('settings.ini')
print(settings.sections())

PATH_DEM20M=settings['dem_data']['PATH_DEM20M']
PATH_GEOTIFF=settings['dem_data']['PATH_GEOTIFF']

service_handler = grass.Service(gisdb = settings['grass']['GISDB'])

def add_cors_headers():
    response.set_header('Access-Control-Allow-Origin', '*')
    response.add_header('Access-Control-Allow-Methods',
                        'GET, POST, PUT, OPTIONS')
    response.set_header('Access-Control-Allow-Headers', 'Content-Type')


@error(500)
def error500(error):
    add_cors_headers()
    return 'Something went wrong here, sorry'


@route('/<:re:.*>', method='OPTIONS')
def enable_cors_generic_route():
    add_cors_headers()


@route('/dem/profile', method='POST')
def dem_profile():
    body = request.body.read().decode("utf-8")
    resolution = request.query.get('resolution', 20)
    add_cors_headers()
    return service_handler.profile(body, resolution)


def main(reloader=False):
    for geotiff in glob.glob(PATH_GEOTIFF + "/**/*.tif"):
        path = pathlib.PurePath(geotiff)
        print('load', geotiff, '...')
        service_handler.load(geotiff, path.name)
    print('load h2beaver base data: DEM-20M', PATH_DEM20M, '...')
    service_handler.load(PATH_DEM20M, 'h2_dem')

    print('prepare to start bottle web server...')
    run(host='0.0.0.0', port=5009, debug=True, reloader=reloader)


if __name__ == '__main__':
    main()
