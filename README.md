# gis-service

To start gis-service locally via virtual env:
1. Install grass 7.8
2. setup virtual-env at projet fodler `python3 -m venv virtual-env`
3. switch to virtual-env `source virtual-env/bin/actiavte`
4. install dependency `python3 -m pip install -r src/main/resources/requirements.txt`
5. Start the service with grass module loaded: `PYTHONPATH=/usr/lib/grass78/etc/python python3 src/main/python/main.py` 


To start gis-service locally via docker:  

1. Build docker image: `docker build -t h2beaver/gis-service .`  
2. Start the service: `docker run -p 5009:5009 h2beaver/gis-service python3 /app/src/main/python/main.py`

Current API End-point:

* POST /dem/profile: provide a geojson LineString object as the body, to get profile from the dem data

  
Example of request:  
`
curl -XPOST -d' { "TYPE": "LineString", "coordinates": [[263263.173026617,2763502.7424211],[263975.312518687,2762594.63036451],[265531.24328866,2761109.64386414],[265546.272369839,2761277.50128893]]}' localhost:5009/dem/profile
`