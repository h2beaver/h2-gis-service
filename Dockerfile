FROM fondus-grass
#FROM mundialis/grass-py3-pdal 
#FROM mundialis/grass-gis-stable

# Install maven and setup grass

USER root
RUN apt update
RUN apt install maven python3-setuptools -y

# prepare for python gis service
RUN pip3 install wheel
RUN pip3 install Cython
RUN pip3 install pyproj~=1.9.6

RUN ln -s /usr/local/grass78 /usr/local/grass

# Environment variable
ENV PYTHONUNBUFFERED=True
ENV GISBASE="/usr/local/grass"
ENV LD_LIBRARY_PATH="$GISBASE/lib"
ENV PYTHONPATH="$GISBASE/etc/python"
ENV PATH="$PATH:$GISBASE/bin"

RUN mkdir /app
RUN mkdir /app/grassdata/
RUN grass -c epsg:3826 -e /app/grassdata/tmp/


# Add pom and download dependency in Docker image
WORKDIR /app
ADD pom.xml /app/
RUN mvn dependency:go-offline

# clone ./src/ into container /app
ADD ./src/ /app/src/

# process h2beaver data
RUN mvn process-resources

RUN pip3 install -r src/main/resources/requirements.txt

# Expose port
EXPOSE 5009

#ENTRYPOINT ["/bin/bash"]
#CMD ["mvn clean compile exec-java"]
